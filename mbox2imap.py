#! /usr/bin/env python
#  vim: set syntax=none:
#######################################################
# Mailbox to imap converter
#######################################################
#         (c) Radim Kolar 2005
#  http://home.tiscali.cz/~cz210552/
#
#   You may copy or modify this file in any manner you wish, provided
#   that this notice is always included, and that you hold the author
#   harmless for any loss or damage resulting from the installation or
#   use of this software.
#
#######################################################
import imaplib
import mailbox
from netrc import netrc
import sys
import urllib.request, urllib.parse, urllib.error
import os
import time
import email.utils

BADMESSAGES = 'failed.txt'

################ main #############
mboxl = None
messages = 0
badmessages = 0

if len(sys.argv) == 1:
    print(sys.argv[0], 'imap://user@host/path filename')
    sys.exit(1)

for arg in sys.argv[1:]:
    if arg.startswith('imap://'):
        arg = arg[5:]
        hostpart,mbox = urllib.parse.splithost(arg)
        mbox=mbox[1:]
        if mbox == '':
            mbox = 'INBOX'
        userpart, hostpart = urllib.parse.splituser(hostpart)
        host,port = urllib.parse.splitnport(hostpart,993)
        if userpart is None:
            userpart = os.getlogin()
        user,passwd = urllib.parse.splitpasswd(userpart)
        if passwd is None:
            try:
               auth = netrc().authenticators(host)
               if auth == None or auth[0] != user:
                   raise IOError
            except IOError:
               print("No password for",user+'@'+host,"in your .netrc file or URL.")
               sys.exit(2)
            passwd = auth[2]
    else:
        mboxl = arg

####### open local mailbox ############

if not os.path.exists(mboxl):
    print("mailbox", mboxl, "does not exists.")
    sys.exit(2)
if os.path.isfile(mboxl):
    mboxl = mailbox.mbox(mboxl)
else:
    mboxl = mailbox.Maildir(mboxl)

#### connect to imap4 server ####
imap = imaplib.IMAP4_SSL(host, port)
imap.login(user, passwd)
print("Mailboxes: ", imap.list())
typ,data = imap.select(mbox)
if typ == 'NO':
    typ,data = imap.create(mbox)
    typ,data = imap.select(mbox)
if typ == 'NO':
    print("Can not create/select imap folder",mbox)
    sys.exit(2)

msgids = []
#### Delete old messages from folder #####
if int(data[0])>0:
#    typ,data=imap.store('1:*', '+FLAGS', '\\Deleted')
#    imap.expunge()
     typ,data = imap.fetch('1:*','(BODY.PEEK[HEADER.FIELDS (Message-Id)])')
     for rp in data:
       if len(rp) < 2:
           continue
       print(rp)
       newmid=rp[1].split()
       if len(newmid) < 1:
           continue
       msgids.append(newmid[1])

now = time.time()
####### Upload all messages from mailbox ########
for m in mboxl:
   try:
        newmid = m.get('Message-id')
        if newmid is not None:
            newmid = newmid.split()[0]
        if not newmid in msgids:
            print('  adding message',newmid)
            imap.append(mbox, None, email.utils.parsedate(m.get('Date')), m.as_bytes())
            messages += 1
   except imap.error:
        badmessages += 1
        m.fp.seek(0)
        f = file(BADMESSAGES,'a')
        f.write("From nobody@nowhere.net Mon Jan 2 00:00:00 1970\n")
        f.write(m.fp.read())
        f.close()

imap.close()
imap.logout()
print(messages, 'inserted,', badmessages, 'failed.', (messages+badmessages)/(time.time()-now), 'per sec.')
