README
======

Python script for converting mailbox or maildir to IMAP folder. I am using this for importing mails into DBMail

Originally from [http://home.tiscali.cz/~cz210552/mailbox2imap.html](http://home.tiscali.cz/~cz210552/mailbox2imap.html)

Hacked around to function with python3(.7)


Usage
-----
First create a file ~/.netrc
```
machine your.imapserver.org
login your@log.in
password p@ssw0rd
```

Then run
```
python mbox2imap.py imap://your@log.in@your.imapserver.org/INBOX Inbox.mbox
python mbox2imap.py imap://your@log.in@your.imapserver.org/Sent Sent.mbox
python mbox2imap.py imap://your@log.in@your.imapserver.org/Drafts Draft.mbox
```
